#! /bin/bash

TARGET="jetons.html"
SRC=jetons.md
cat > $TARGET <<EOF
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <style>
      html { margin: 0; }
      body{
          -webkit-print-color-adjust:exact !important;
          print-color-adjust:exact !important;
	  margin: 0;
      }
      #jetons {
          display: grid;
          max-width: 20cm;
          grid-template-columns: repeat(4, 1fr);
#          gap: 0.3cm;
      }
      .face, .pile {
	  page-break-inside: avoid;
	  --width: 5.5cm;
	  --height: 5.5cm;
	  display: flex;
	  flex-direction: column;
	  align-items: center;
	  justify-content: center;
          border: 1px solid var(--fg);
          background-color: var(--bg);
          width: var(--width);
          height: var(--height);
          max-width: var(--width);
          max-height: var(--height);
          min-width: var(--width);
          min-height: var(--height);
          text-align: center;
          margin: 0.5cm;
          border-radius:0.2cm;
          box-sizing: border-box;
          color: var(--fg);
      }
      .face {
          --fg:hsl(180 50% 20%);
          --bg:hsl(180 50% 100%);
	  font-size: 1.2em;
      }
      .face img {
          width: 4cm;
          max-width: 4cm;
          max-height: 4cm;
          height: 4cm;
          
      }
      .color3, .color4 {
          --fg:hsl(0 50% 50%);
          --bg:hsl(0 50% 90%);
      }
      .color2 {
          --fg:hsl(30 50% 50%);
          --bg:hsl(30 50% 90%);
      }
      .color1, .color0 {
          --fg:hsl(100 50% 50%);
          --bg:hsl(100 50% 90%);
      }
      .miniature {
      	  max-width:1cm;
      	  max-height:1cm;
          position:absolute; top:0.1cm; right:0.1cm;

       }

          
      .pile {
          font-size: 1.2cm;
          padding-top:0.8cm;
          padding-bottom:0.6cm;
          text-align: center;
	  position:relative;
      }
    </style>
  </head>
  <body>
    <div id="jetons">
EOF
cat $SRC | egrep '^#' | while read line; do
    TITLE=$(echo $line | cut -d'|' -f 1 | sed 's/# //g')
    FOOTPRINT=$(echo $line | cut -d'|' -f 2)
    PIC=$(echo $line | cut -d'|' -f 3)
    PERCENT=$(expr "$FOOTPRINT" / 20)
    if test "$FOOTPRINT" -gt 500; then
	PRETTY_FOOTPRINT="$(echo '' | awk "{printf(\"%0.1f T\", $FOOTPRINT / 1000);}")";
    elif test "$FOOTPRINT" -gt 1; then
	PRETTY_FOOTPRINT="$FOOTPRINT kg";
    fi
    COLOR="$(echo '' | awk "{printf(\"color%d\", log($FOOTPRINT)/log(10))}")"
    echo "<span class=\"face\"><img src=\"$PIC\" /> <br /><i>$TITLE</i></span>"
    echo "<span class=\"pile $COLOR\"><img class=\"miniature\" src=\"$PIC\" />$PRETTY_FOOTPRINT [$PERCENT%]</span>";
done >> $TARGET

cat >> $TARGET <<EOF
    </div>
  </body>
</html>
EOF
