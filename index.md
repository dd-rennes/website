---
title: GT missions Rennes / Groupe DD Rennes
---

## Contexte

Le GT missions de Rennes a été recréé en septembre 2022. Sa mission première est
de réfléchir et de proposer des scénarios pour limiter les missions en avion au
niveau du centre de Rennes.

## Programme de l'automne 2023

Nous avons prévu pour l'automne 2023 une série d'évènement pour
communiquer sur le bilan du laboratoire, et amorcer une réflexion sur
la réduction des missions à impacts forts. Trois temps sont prévus:

- **21 septembre, 9h**: Présentation du bilan (Amphi Inria)
- **12 octobre, 9h**: Atelier « Réduction des mission » (Pétri-Turing)
- **14 décembre, 14h**: Agora (Amphi Inria)

## Actions

- BGES du centre
    - Objectifs: donner les ordres de grandeur de nos activités professionnelles (dont les missions)
    - Comment: en utilisant les outils du labos1point5

- Sensibilisation:
    - Objectifs: augmenter la prise de conscience auprès des collègues de l'impact de nos activités
    - Comment: organisation d'événements de type fresques (climat, numérique),
    happening à la cafét', atelier de prospective ...

- Scénario:
    - Objectifs: mettre au point des scénario de réduction de l'impact des missions
    - Comment: en lien avec la sensibilisation, rédaction puis présentation des scénarios possibles.

## Nous rejoindre

- mailing list: [s'inscrire](mailto:sympa_inria@inria.fr?subject=subscribe%20dd-rba)
- channel [mattermost](https://mattermost.inria.fr/dd-rba/channels/town-square)

## Actions connexes

- [Groupe SEnS Rennes](https://sens-rennes.gitlabpages.inria.fr/)
