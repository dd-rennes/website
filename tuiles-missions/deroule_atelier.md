---
title: Déroulé de l'atelier « Quelles missions choisir »
---

# Atelier collaboratif "réduire l'empreinte carbone de nos missions"
12 Octobre 2023, centre Inria / IRISA de Rennes

1. présentation du déroulé de la demi-journée (15 min.)
2. Jeu de tuiles (60 min.)
3. pause (15 min.)
4. tables rondes (60 min.)
5. Résumé et conclusions (15 min.)

## Introduction (~15 min -- 9h-9h15)

 - *prévoir organisation de la salle à l'avance : mise en place des tables et installation des jeux*
 - *On installe les gens à leur table prédéfinies à l'avance. Prévoir un affichage ?*
 - 3 salles:
   - Pétri-Turing
   - Salle Markov
   - Salle direction

Introduction **debout** en Pétri-Turing. Animateurs face aux personnels.

### rappel du contexte 
La France est engagée dans un démarche de réduction de son empreinte carbone, visant à atteindre une "neutralité carbone" de 2 TCO2e/pers/an en 2050 (*vs* 10 T aujourd'hui). Dans cette trajectoire, un premier objectif est de réduire nos émissions de 40% d'ici 2030 (par rapport à 1990). Dans ce bilan, la recherche académique et l'enseignement supérieur émargent au titre des services publiques, qui valent pour 1.4 T. Etant donnés l'urgence de la transition à opérer et la morphologie des émissions de CO2, une approche globale est nécessaire, à la fois concernant les secteurs, mais aussi les acteurs. En somme : tout le monde est concerné.

En moyenne (Labo1p5), les voyages professionnels pour mission représentent 1/4 de nos émissions (largement dominés par l'avion) et présentent de fortes disparités (entre les structures de recherches, mais aussi entre les individus). Au centre Inria/IRISA de Rennes, les émissions par personne et par an varient typiquement entre 0.4 TCO2e (moyenne PhD) et 2 TCO2e (moyenne rang A).

### But de l'atelier et déroulé
But : faire émerger des propositions et mesures envisageables afin de faire diminuer l'empreinte carbone de nos déplacements professionnels (missions), et en mesurer l'acceptabilité.
L'atelier est organisé en **2 phases**:

1. jeu de tuiles mettant en scène une équipe-projet fictive et le panorama des missions réalisées par ses membres (tables de 5 personnes)
2. atelier par groupes (15 pers environ) visant à co-construire des règles
3. Conclusions / synthèse

### Dispatch dans les salles
Chaque référents prend ses personnes dans sa salle.

## Jeu de tuiles (table de 5 pers. -- 60 min. -- 9h15-10h15)
*1 animateur par table est souhaité. Sinon, prévoir 1 ou plusieurs animateurs "flottants". Matériel : jeu de tuile + fiche de synthèse*

 - *5 minutes tour de table : nom, statut, équipe... animateur inclus qui précise son rôle dans cette phase*
 - *10 minutes : explication des règles du jeu*

### Règles du jeu :
Les participants disposent d'un jeu de tuiles qui correspondent à des missions en avions typiques réalisées sur une durée de trois ans par une petite équipe de recherche : **1 rang A (Pr/Dr), 2 rang B (MCF/CR), 1 postdoc, 1/2 ingé, 4 PhDs** (missions et équipe inspirés de données locales). Chaque tuile représente une mission, sa couleur indique le statut de la personne effectuant la mission, et la surface de la tuile est proportionnelle à l'émission de GES associée. La surface occupée initialement correspond donc à l'émission totale de l'équipe (~8.3 TCO2eq/an). Le but est de faire diminuer ces émissions -- *i.e.* la surface occupée par les tuiles : il faudra donc faire des choix **motivés**.

Le jeu se déroule en 2 manches de 20 minutes chacune : réduction à 70% (-30%), puis réduction à 50% (-30%). Au cours des parties, l'animateur (ou les joueurs) remplit le tableau "phase de jeu" afin de garder une trace des tuiles retirées, en consignant éventuellement les motivations à l'origine de ces choix ou les remarques faites. **Si un choix ne fait pas l'unanimité, cela peut être renseigné dans la colonne "vote"** (pour/total).

A l'issue de chaque manche, les joueurs tentent d'établir des règles/propositions/mesures qui pourraient émerger de la manche jouée. Ils (ou l'animateur) remplissent le tableau "synthèse après manche".

 - *Installation du jeu: on dispose les tuiles en 5 lignes sur une feuille paperboard prédécoupée. Sur cette feuille, 2 lignes verticales indiquent le trait de découpe permettant d'appliquer les réduction d'empreinte*
 - *on peut prendre 25 minutes pour première manche, 20 minutes pour seconde, synthèse inclues*
 - *N.B.: chaque table a un jeu de tuiles différent*

## - -  Pause (15 min. -- 10h15-10h30)  - -
Café et croissants seront à disposition. *Les animateurs réorganisent la salle -- pas l'temps d'niaiser...*

## Phase 2 : construction de propositions (groupes de 15 pers. -- 60 min. -- 10h30-11h30)
*Les tables sont fusionnées par 3. Il faut impérativement un animateur au moins par groupe. Matériel: fiche de synthèse*

En s'aidant des tableaux renseignés au cours de la première phase, les participants tentent d'élaborer différentes mesures envisageables visant à réduire l'empreinte carbone des missions. **Il n'est pas nécessaire d'obtenir un concensus et les propositions élaborées peuvent présenter des failles :** le tableau synthétique permet de renseigner la popularité (vote) et les potentielles lacunes.

1. Présentation de la phase et des objectifs : Formaliser et fusionner les règles qui ressortent de la phase de jeu. But : aboutir à des mesures concrètes d'organisation et discuter de leur applicabilité (5 minutes)
2. Synthèse/restitution des jeux de tuile (5 min/table = 15 minutes)
3. discussion et élaboration de mesures (35 minutes)
4. synthèse: identifier, en quelques points, des propositions "phares" (5 minutes)

 - *Elements de reflexion: discuter l'efficacité vs. contrainte, l'implémentation, les variantes, etc., (comment, facile/difficile), de leur efficacité potentielle (% réduction visé), etc.*
 - *l'animateur veille à ce que le groupe construise des mesures concrètes et reportent dans la fiche de synthèse*

**Quelques propositions qui peuvent être soumises au groupe en cas de creux : [antisèches](antiseches.html)**

 - *quotas: dépassement possible ? échelle individuelle, équipe, labo ? fenêtre en temps glissante ? Different par statut (e.g. priorité jeunes chercheurs) ?*
 - *taxe carbone sur les trajets en avion (financement du train)*
 - *report modal de tout voyage <X h en train*
 - *update des plateformes de réservation*
 - *confs en hybride? hubs?*

## Conclusions / résumés (11h30-12h)
* Restitution synthétique des groupes sur les quelques mesures phares identifiées (5 min/groupe = 15 minutes)
* *Octogone*
* merci tout le monde et rappel des actions à venir (utilisation des infos récoltées pour formuler des scénarios qui seront débattus en Agora et soumis au vote suite à l'Agora)

